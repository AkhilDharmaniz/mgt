//
//  SignUpVC.swift
//  racinewalker
//
//  Created by Vivek Dharmani on 6/24/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit
import AVFoundation

class SignUpVC: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var passwordTxtFld: UITextField!
    @IBOutlet weak var emailTxtFld: UITextField!
    @IBOutlet weak var lastNameTxtFld: UITextField!
    @IBOutlet weak var firstNameTxtFld: UITextField!
    var selectedAgree = false
    var avPlayer: AVPlayer!
    var avPlayerLayer: AVPlayerLayer!
    override func viewDidLoad() {
        super.viewDidLoad()
        toPlayVideo()
        
        // Do any additional setup after loading the view.
    }
    
    @objc func playerItemDidReachEnd(notification: Notification) {
        let p: AVPlayerItem = notification.object as! AVPlayerItem
        p.seek(to: CMTime.zero, completionHandler: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        avPlayer.play()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        avPlayer.pause()
    }
    @IBAction func agreeBtnAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        selectedAgree = sender.isSelected
        print(selectedAgree)
    }
    
    @IBAction func termsAndConditionsBtnAction(_ sender: UIButton) {
       let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TermsOfUseVC") as? TermsOfUseVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    @IBAction func signUpButtonAction(_ sender: Any) {
//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBarVC") as? TabBarVC
//        self.navigationController?.pushViewController(vc!, animated: true)
        
//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC
//        self.navigationController?.pushViewController(vc!, animated: true)
        
        if (firstNameTxtFld.text?.isEmpty)!{
            
            ValidateData(strMessage: "Please enter first name")
        }
        else if (lastNameTxtFld.text?.isEmpty)!{
            
            ValidateData(strMessage: "Please enter last name")
        }
        else if (emailTxtFld.text?.isEmpty)!{
            
            ValidateData(strMessage: "Please enter email address")
        }
        else if isValidEmail(testStr: (emailTxtFld.text)!) == false{
            
            ValidateData(strMessage: "Enter valid email")
        }
        else if (passwordTxtFld.text?.isEmpty)!{
            
            ValidateData(strMessage: "Please enter password")
        }else if (passwordTxtFld!.text!.count) < 4 || (passwordTxtFld!.text!.count) > 15{
            
            ValidateData(strMessage: "Please enter minimum 4 digit password")
            //            UserDefaults.standard.set(passwordTxtFld.text, forKey: "password")
            UserDefaults.standard.string(forKey: "password")
        }else if selectedAgree == false{
            
            ValidateData(strMessage: "Please agree terms and conditions")
        }
        else{
            signUp()
        }
    }
    
        func signUp() {
            if Reachability.isConnectedToNetwork() == true {
                print("Internet connection OK")
                IJProgressView.shared.showProgressView()
                let signInUrl = Constant.shared.baseUrl + Constant.shared.SignUp
                print(signInUrl)
                var deviceID = UserDefaults.standard.value(forKey: "device_token") as? String
                print(deviceID ?? "")
                if deviceID == nil  {
                    deviceID = "777"
                }
                let date = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                print(dateFormatter.string(from: date))
                let currentDate = dateFormatter.string(from: date)
                let parms : [String:Any] = ["first_name":firstNameTxtFld.text!,
                                            "password":passwordTxtFld.text!,
                                            "email":emailTxtFld.text!,
                                            "last_name":lastNameTxtFld.text!,
                                            "purchasePlan":"trial",
                                            "expiredatetime":currentDate,
                                            "device_type":"1",
                                            "device_token":deviceID ?? ""]
                print(parms)
                AFWrapperClass.requestPOSTURL(signInUrl, params: parms, success: { (response) in
                    IJProgressView.shared.hideProgressView()
                    print(response)
                    let displayMessage = response["message"] as? String ?? ""
                    if let status = response["status"] as? Int{
                        if status == 410{
                            print(response)
                            let allData = response as? [String:Any] ?? [:]
                            print(allData)
                            if let dataDict = allData["user_detail"] as? [String:Any]{
                                print(dataDict)
                                UserDefaults.standard.set(dataDict["id"], forKey: "id")
                                print(dataDict)
                            }
                            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC
                            self.navigationController?.pushViewController(vc!, animated: true)
                        }else if status == 1{
                            showAlertMessage(title: Constant.shared.appTitle, message: displayMessage, okButton: "OK", controller: self) {
                                print(response)
                                let allData = response as? [String:Any] ?? [:]
                                print(allData)
                                if let dataDict = allData["data"] as? [String:Any]{
                                    print(dataDict)
                                    UserDefaults.standard.set(dataDict["id"], forKey: "id")
                                    print(dataDict)
                                }
                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBarVC") as? TabBarVC
                                self.navigationController?.pushViewController(vc!, animated: true)
                            }
                            
                            
                        }else{
                            IJProgressView.shared.hideProgressView()
                            alert(Constant.shared.appTitle, message: displayMessage, view: self)
                        }
                    }
                }) { (error) in
                    IJProgressView.shared.hideProgressView()
                    alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                    print(error)
                }
                
            } else {
                   print("Internet connection FAILED")
                   alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
               }
       
    }
    
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func gotoSignInVC(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func toPlayVideo()  {
        let theURL = Bundle.main.url(forResource:"compresed video", withExtension: "mp4")
        
        avPlayer = AVPlayer(url: theURL!)
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
        avPlayerLayer.videoGravity = .resizeAspectFill
        avPlayer.volume = 0
        avPlayer.actionAtItemEnd = .none
        
        avPlayerLayer.frame = view.layer.bounds
        view.backgroundColor = .clear
        view.layer.insertSublayer(avPlayerLayer, at: 0)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: .AVPlayerItemDidPlayToEndTime,
                                               object: avPlayer.currentItem)
    }
}
