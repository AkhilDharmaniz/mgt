//
//  SignInVC.swift
//  racinewalker
//
//  Created by Vivek Dharmani on 6/24/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit
import AVFoundation
import GoogleSignIn
import FBSDKLoginKit
import FirebaseAuth
import AuthenticationServices

class SignInVC: UIViewController ,GIDSignInDelegate{
    
    var avPlayer: AVPlayer!
    var avPlayerLayer: AVPlayerLayer!
    var message = String()
    var firstName = String()
    var lastName = String()
    var provider : OAuthProvider!
    var currentDate = ""
    @IBOutlet weak var emailTxtFld: UITextField!
    @IBOutlet weak var passwordTxtFld: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        print(dateFormatter.string(from: date))
        currentDate = dateFormatter.string(from: date)
        toPlayVideo()
        GIDSignIn.sharedInstance().delegate = self
        Auth.auth()
        self.provider = OAuthProvider(providerID: "twitter.com")
        provider.customParameters = [
            "lang": "en"
        ]
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let email = user.profile.email
        let picture = user.profile.imageURL(withDimension: 500)
    //    let userInfo = ["userId":userId,"tokenId":idToken,"name":fullName,"email":email, "picture":picture?.absoluteString ?? ""]
        googleLoginApi(firstName: givenName ?? "",lastName: familyName  ?? "", email: email ?? "", googleId: userId ?? "", picture: picture?.absoluteString ?? "")
     //   alert(Constant.shared.appTitle, message: "coming soom", view: self)
        
    }
    func googleLoginApi(firstName: String, lastName: String, email: String, googleId: String, picture: String) {
        if Reachability.isConnectedToNetwork() == true {
            IJProgressView.shared.showProgressView()
            let signUpUrl = Constant.shared.baseUrl + Constant.shared.AddGooglePlusToken
            var deviceID = UserDefaults.standard.value(forKey: "device_token") as? String
            print(deviceID ?? "")
            if deviceID == nil  {
                deviceID = "777"
            }
            let parms : [String:Any] = ["email": email,"google_token": googleId,"first_name": firstName,"last_name": lastName, "image":picture, "device_token": deviceID ?? "", "device_type" : "1", "purchasePlan":"trial", "expiredatetime":currentDate]
            print(parms)
            
            AFWrapperClass.requestPOSTURL(signUpUrl, params: parms, success: { (response) in
                IJProgressView.shared.hideProgressView()
                print(response)
                self.message = response["message"] as? String ?? ""
                if let status = response["status"] as? Int{
                    if status == 1{
                        print(response)
                        let allData = response as? [String:Any] ?? [:]
                        print(allData)
                        if let dataDict = allData["data"] as? [String:Any]{
                            print(dataDict)
                            UserDefaults.standard.set(dataDict["id"], forKey: "id")
                            print(dataDict)
                        }
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBarVC") as? TabBarVC
                        self.navigationController?.pushViewController(vc!, animated: true)
                        
                    }else if status == 410{
                        print(response)
                        let allData = response as? [String:Any] ?? [:]
                        print(allData)
                        if let dataDict = allData["user_detail"] as? [String:Any]{
                            print(dataDict)
                            UserDefaults.standard.set(dataDict["id"], forKey: "id")
                            print(dataDict)
                        }
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constant.shared.appTitle, message: self.message, view: self)
                    }
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                print(error)
            }
            
        } else {
            print("Internet connection FAILED")
            alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
        }
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }

    
    @IBAction func googleSignIn(_ sender: GIDSignInButton) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
//        if (GIDSignIn.sharedInstance()?.hasPreviousSignIn())!{
//            GIDSignIn.sharedInstance()?.restorePreviousSignIn()
//        }else{
            GIDSignIn.sharedInstance()?.signIn()
            
//        }
    }
    
    
    
    @IBAction func fbSignInButtonAction(_ sender: Any) {
        let loginManager = LoginManager()
              loginManager.logOut()
              loginManager.logIn(permissions: ["email"], from: self) { (result, error) in
                  if error == nil{
                      let fbloginresult : LoginManagerLoginResult = result!
                      if(fbloginresult.grantedPermissions.contains("email"))
                      {
                          self.getFBUserData()
                      }
                  }else{
                      print("Failed to login: \(error?.localizedDescription ?? "")")
                      return
                  }
                  guard let accessToken = AccessToken.current else {
                      print("Failed to get access token")
                      return
                  }
//        FacebookSignInManager.basicInfoWithCompletionHandler(self) { (dataDictionary:Dictionary<String, AnyObject>?, error:NSError?) -> Void in
//            print(dataDictionary)
//            if error == nil{
//                alert(Constant.shared.appTitle, message: "coming soom", view: self)
//
//            }else{
//                alert(Constant.shared.appTitle, message: error?.localizedDescription ?? "", view: self)
//            }
//
        }
    }
    func getFBUserData(){
               self.view.isUserInteractionEnabled = false
               IJProgressView.shared.showProgressView()
               if((AccessToken.current) != nil){
                   GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                       if (error == nil){
                           let dict = result as! [String : AnyObject]
                           print(dict)
                           if let dict = result as? [String : AnyObject]{
                               if(dict["email"] as? String == nil || dict["id"] as? String == nil || dict["email"] as? String == "" || dict["id"] as? String == "" ){
                                   
                                   self.view.isUserInteractionEnabled = true
                                   IJProgressView.shared.hideProgressView()
                                   alert(Constant.shared.appTitle, message: "You cannot login with this facebook account because your facebook is not linked with any email", view: self)
                                   
                               }else{
                                   IJProgressView.shared.hideProgressView()
                                   let email = dict["email"] as? String
                                   let first_name = dict["first_name"] as? String
                                   let last_name = dict["last_name"] as? String
                                   var image = ""
                                if let picture = dict["picture"] as? [String:Any]{
                                    if let data = picture["data"] as? [String:Any]{
                                        if let url = data["url"] as? String{
                                            image = url
                                        }
                                    }
                                }
                                   let id = dict["id"] as? String
                                 
                                self.fbLogInApi(firstName: first_name ?? "",lastName: last_name  ?? "", email: email ?? "", fbId: id ?? "", picture: image)
                               }
                           }
                           
                       }else{
                           self.view.isUserInteractionEnabled = true
                           IJProgressView.shared.hideProgressView()
                           
                           
                       }
                   })
               }
               
           }
    func fbLogInApi(firstName: String,lastName: String, email: String, fbId: String, picture: String) {
          if Reachability.isConnectedToNetwork() == true {
              IJProgressView.shared.showProgressView()
              let signUpUrl = Constant.shared.baseUrl + Constant.shared.AddFacebookToken
              var deviceID = UserDefaults.standard.value(forKey: "device_token") as? String
              print(deviceID ?? "")
              if deviceID == nil  {
                  deviceID = "777"
              }
              let parms : [String:Any] = ["email": email,"fb_token": fbId,"first_name": firstName,"last_name": lastName, "image":picture, "device_token": deviceID ?? "", "device_type" : "1", "purchasePlan":"trial", "expiredatetime":currentDate]
              print(parms)
              AFWrapperClass.requestPOSTURL(signUpUrl, params: parms, success: { (response) in
                  IJProgressView.shared.hideProgressView()
                  print(response)
                  self.message = response["message"] as? String ?? ""
                  if let status = response["status"] as? Int{
                      if status == 1{
                          print(response)
                          let allData = response as? [String:Any] ?? [:]
                          print(allData)
                          if let dataDict = allData["data"] as? [String:Any]{
                              print(dataDict)
                              UserDefaults.standard.set(dataDict["id"], forKey: "id")
                              print(dataDict)
                          }
                          let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBarVC") as? TabBarVC
                          self.navigationController?.pushViewController(vc!, animated: true)
                          
                      }else if status == 410{
                          print(response)
                          let allData = response as? [String:Any] ?? [:]
                          print(allData)
                          if let dataDict = allData["user_detail"] as? [String:Any]{
                              print(dataDict)
                              UserDefaults.standard.set(dataDict["id"], forKey: "id")
                              print(dataDict)
                          }
                          let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC
                          self.navigationController?.pushViewController(vc!, animated: true)
                      }else{
                          IJProgressView.shared.hideProgressView()
                          alert(Constant.shared.appTitle, message: self.message, view: self)
                      }
                  }
              }) { (error) in
                  IJProgressView.shared.hideProgressView()
                  alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                  print(error)
              }
              
          } else {
              print("Internet connection FAILED")
              alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
          }
          
      }
    
    
    @IBAction func twitterLoginButtonAction(_ sender: Any) {
           provider.getCredentialWith(nil) { credential, error in
            if error != nil {
                // Handle error.
                if let msg : String = error?.localizedDescription ?? "" {
                    if msg != "The interaction was cancelled by the user." {
                        alert(Constant.shared.appTitle, message: error?.localizedDescription ?? "", view: self)
                        print(error)
                    }
                    DispatchQueue.main.async {
                        IJProgressView.shared.hideProgressView()
                    }
                }
                return;
            }
                    IJProgressView.shared.showProgressView()
                  if let ctr = credential  {
                    Auth.auth().signIn(with: ctr) { authResult, error in
                        if error != nil {
                            // Handle error.
                            alert(Constant.shared.appTitle, message: error?.localizedDescription ?? "", view: self)
                            DispatchQueue.main.async {
                                IJProgressView.shared.hideProgressView()
                            }
                            return
                        }
                        print(authResult?.additionalUserInfo?.profile?["email"] as? String ?? "");
                        print(authResult?.additionalUserInfo?.profile?["name"] as? String ?? "");

                        print(authResult?.additionalUserInfo?.profile?["id"] as? String ?? "");

                        print(authResult?.additionalUserInfo?.profile?["profile_image_url"] as? String ?? "");
                           var twitterID = authResult?.additionalUserInfo?.profile?["id_str"] as? String ?? ""
                        if twitterID == "" {
                            if let idInt = authResult?.additionalUserInfo?.profile?["id"] as? Int {
                          twitterID =  String(idInt)
                            }
                        }
                        self.twitterLogInApi(firstName: authResult?.additionalUserInfo?.profile?["name"] as? String ?? "", lastName: "" , email: authResult?.additionalUserInfo?.profile?["email"] as? String ?? "" , twitterId: twitterID , picture: authResult?.additionalUserInfo?.profile?["profile_image_url"] as? String ?? "")
                      // User is signed in.
                      // IdP data available in authResult.additionalUserInfo.profile.
                      // Twitter OAuth access token can also be retrieved by:
                      // authResult.credential.accessToken
                      // Twitter OAuth ID token can be retrieved by calling:
                      // authResult.credential.idToken
                      // Twitter OAuth secret can be retrieved by calling:
                      // authResult.credential.secret
                    }
                  }
   
//        TWTRTwitter.sharedInstance().logIn { (session, error) in
//            if (session != nil) {
//                let firstName = session?.userName ?? ""
//                let userId = session?.userID ?? ""
//                var recivedEmailID = ""
//                self.dismiss(animated: true) {
//                    print("Logged in ")
//                }
//                let client = TWTRAPIClient.withCurrentUser()
//                client.requestEmail { email, error in
//                    if (email != nil) {
//                        print("signed in as \(String(describing: session?.userName))");
//                       // let firstName = session?.userName ?? ""   // received first name
//                        recivedEmailID = email ?? ""   // received email
//                       // alert(Constant.shared.appTitle, message: "coming soom", view: self)
//                    }else {
//                        print("error: \(String(describing: error?.localizedDescription))");
//                    }
//                }
//                var imageUrl = ""
//                client.loadUser(withID: session!.userID, completion: { (userData, error) in
//                    if (userData != nil) {
//                        imageUrl = userData?.profileImageLargeURL ?? "" //User Profile Image
//                        let userTwitterProfileUrl = userData?.profileURL // User TwitterProfileUrl
//                    }
//                })
//
//                self.twitterLogInApi(firstName: firstName ,lastName: "", email: recivedEmailID , twitterId: userId , picture: imageUrl)
//            }else {
//                print("error: \(String(describing: error?.localizedDescription))");
//            }
        }
    }
    func twitterLogInApi(firstName: String,lastName: String, email: String, twitterId: String, picture: String) {
             if Reachability.isConnectedToNetwork() == true {
                 IJProgressView.shared.showProgressView()
                 let signUpUrl = Constant.shared.baseUrl + Constant.shared.AddTwitterToken
                 var deviceID = UserDefaults.standard.value(forKey: "device_token") as? String
                 print(deviceID ?? "")
                 if deviceID == nil  {
                     deviceID = "777"
                 }
                 let parms : [String:Any] = ["email": email,"twitter_token": twitterId,"first_name": firstName,"last_name": lastName, "image":picture, "device_token": deviceID ?? "", "device_type" : "1", "purchasePlan":"trial", "expiredatetime":currentDate]
                 print(parms)
                 
                 AFWrapperClass.requestPOSTURL(signUpUrl, params: parms, success: { (response) in
                     IJProgressView.shared.hideProgressView()
                     print(response)
                     self.message = response["message"] as? String ?? ""
                     if let status = response["status"] as? Int{
                         if status == 1{
                             print(response)
                             let allData = response as? [String:Any] ?? [:]
                             print(allData)
                             if let dataDict = allData["data"] as? [String:Any]{
                                 print(dataDict)
                                 UserDefaults.standard.set(dataDict["id"], forKey: "id")
                                 print(dataDict)
                             }
                             let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBarVC") as? TabBarVC
                             self.navigationController?.pushViewController(vc!, animated: true)
                             
                         }else if status == 410{
                             print(response)
                             let allData = response as? [String:Any] ?? [:]
                             print(allData)
                             if let dataDict = allData["user_detail"] as? [String:Any]{
                                 print(dataDict)
                                 UserDefaults.standard.set(dataDict["id"], forKey: "id")
                                 print(dataDict)
                             }
                             let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC
                             self.navigationController?.pushViewController(vc!, animated: true)
                         }else{
                             IJProgressView.shared.hideProgressView()
                             alert(Constant.shared.appTitle, message: self.message, view: self)
                         }
                     }
                 }) { (error) in
                     IJProgressView.shared.hideProgressView()
                     alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                     print(error)
                 }
                 
             } else {
                 print("Internet connection FAILED")
                 alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
             }
             
         }
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
//    }
    
    @IBAction func appleLogInBtnAction(_ sender: UIButton) {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.performRequests()
        } else {
            DispatchQueue.main.async {
                alert(Constant.shared.appTitle, message: "Apple login support in iOS 13 and above", view: self)
            }
            
        }
    }
    @objc func playerItemDidReachEnd(notification: Notification) {
        let p: AVPlayerItem = notification.object as! AVPlayerItem
        p.seek(to: CMTime.zero, completionHandler: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        avPlayer.play()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        avPlayer.pause()
    }
    @IBAction func gotoSignUpView(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignUpVC") as? SignUpVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func forgotPasswordButtonAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ForgotPasswordVC") as? ForgotPasswordVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    
    @IBAction func logInButtonAction(_ sender: Any) {
        
        
        if (emailTxtFld.text?.isEmpty)!{
            
            ValidateData(strMessage: " Please enter email")
        }
        else if isValidEmail(testStr: (emailTxtFld.text)!) == false{
            
            ValidateData(strMessage: "Enter valid email")
        }
        else if (passwordTxtFld.text?.isEmpty)!{
            
            ValidateData(strMessage: " Please enter password")
        }else{
            self.getData()
        }
    }
    
    func toPlayVideo()  {
        let theURL = Bundle.main.url(forResource:"compresed video", withExtension: "mp4")
        
        avPlayer = AVPlayer(url: theURL!)
        avPlayerLayer = AVPlayerLayer(player: avPlayer)
        avPlayerLayer.videoGravity = .resizeAspectFill
        avPlayer.volume = 0
        avPlayer.actionAtItemEnd = .none
        
        avPlayerLayer.frame = view.layer.bounds
        view.backgroundColor = .clear
        view.layer.insertSublayer(avPlayerLayer, at: 0)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: .AVPlayerItemDidPlayToEndTime,
                                               object: avPlayer.currentItem)
    }
    
    func getData() {
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            IJProgressView.shared.showProgressView()
            let signInUrl = Constant.shared.baseUrl + Constant.shared.signIn
            print(signInUrl)
            var deviceID = UserDefaults.standard.value(forKey: "device_token") as? String
            print(deviceID ?? "")
            if deviceID == nil  {
                deviceID = "777"
            }
            let parms : [String:Any] = ["email":emailTxtFld.text ?? "",
                                        "password":passwordTxtFld.text ?? "",
                                        "purchasePlan":"",
                                        "expiredatetime":"",
                                        "device_type":"1",
                                        "device_token":deviceID ?? ""]
            print(parms)
            AFWrapperClass.requestPOSTURL(signInUrl, params: parms, success: { (response) in
                IJProgressView.shared.hideProgressView()
                print(response)
                self.message = response["message"] as? String ?? ""
                if let status = response["status"] as? Int{
                    if status == 1{
                        print(response)
                        let allData = response as? [String:Any] ?? [:]
                        print(allData)
                        if let dataDict = allData["data"] as? [String:Any]{
                            print(dataDict)
                            UserDefaults.standard.set(dataDict["id"], forKey: "id")
                            print(dataDict)
                        }
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBarVC") as? TabBarVC
                        self.navigationController?.pushViewController(vc!, animated: true)
                        
                    }else if status == 410{
                        print(response)
                        let allData = response as? [String:Any] ?? [:]
                        print(allData)
                        if let dataDict = allData["user_detail"] as? [String:Any]{
                            print(dataDict)
                            UserDefaults.standard.set(dataDict["id"], forKey: "id")
                            print(dataDict)
                        }
                     let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC
                     self.navigationController?.pushViewController(vc!, animated: true)
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constant.shared.appTitle, message: self.message, view: self)
                    }
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                print(error)
            }
            
        } else {
            print("Internet connection FAILED")
            alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
        }
    }
    func appleLoginServiceCall(email:String,firstName:String,lastName:String,appleID:String) {
        if Reachability.isConnectedToNetwork() == true {
            IJProgressView.shared.showProgressView()
            let signUpUrl = Constant.shared.baseUrl + Constant.shared.AddAppleToken
            var deviceID = UserDefaults.standard.value(forKey: "device_token") as? String
            print(deviceID ?? "")
            if deviceID == nil  {
                deviceID = "777"
            }
            let parms : [String:Any] = ["email": email,"apple_token": appleID,"first_name": firstName,"last_name": lastName, "image":"", "device_token": deviceID ?? "", "device_type" : "1", "purchasePlan":"trial", "expiredatetime":currentDate]
            print(parms)
            
            AFWrapperClass.requestPOSTURL(signUpUrl, params: parms, success: { (response) in
                IJProgressView.shared.hideProgressView()
                print(response)
                self.message = response["message"] as? String ?? ""
                if let status = response["status"] as? Int{
                    if status == 1{
                        print(response)
                        let allData = response as? [String:Any] ?? [:]
                        print(allData)
                        if let dataDict = allData["data"] as? [String:Any]{
                            print(dataDict)
                            UserDefaults.standard.set(dataDict["id"], forKey: "id")
                            print(dataDict)
                        }
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBarVC") as? TabBarVC
                        self.navigationController?.pushViewController(vc!, animated: true)
                        
                    }else if status == 410{
                        print(response)
                        let allData = response as? [String:Any] ?? [:]
                        print(allData)
                        if let dataDict = allData["user_detail"] as? [String:Any]{
                            print(dataDict)
                            UserDefaults.standard.set(dataDict["id"], forKey: "id")
                            print(dataDict)
                        }
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constant.shared.appTitle, message: self.message, view: self)
                    }
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                print(error)
            }
            
        } else {
            print("Internet connection FAILED")
            alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
        }
        
    }
}
extension SignInVC : ASAuthorizationControllerDelegate {
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let userFirstName = appleIDCredential.fullName?.givenName
            let userLastName = appleIDCredential.fullName?.familyName
            print(userFirstName,userLastName,fullName)
            if let email = appleIDCredential.email, let _ = appleIDCredential.fullName  {
                UserDefaults.standard.setValue(userIdentifier, forKey: "appleId")
                UserDefaults.standard.setValue(userFirstName, forKey: "appleFirstName")
                UserDefaults.standard.setValue(userLastName, forKey: "appleLastName")
                UserDefaults.standard.setValue(email, forKey: "appleEmail")
                self.appleLoginServiceCall(email: email, firstName: userFirstName ?? "",lastName: userLastName ?? "", appleID: userIdentifier)
            }else {
                let userId = UserDefaults.standard.value(forKey: "appleId") as? String
                var first_name = ""
                var Last_name = ""
                var email = ""
                if userIdentifier == userId {
                    first_name = UserDefaults.standard.value(forKey: "appleFirstName") as? String ?? ""
                    Last_name = UserDefaults.standard.value(forKey: "appleLastName")  as? String ?? ""
                    email = UserDefaults.standard.value(forKey: "appleEmail") as? String ?? ""
                }
                self.appleLoginServiceCall(email: email, firstName: first_name,lastName: Last_name, appleID: userIdentifier)
            }
        }
    }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        DispatchQueue.main.async {
            alert(Constant.shared.appTitle, message:error.localizedDescription, view: self)
             IJProgressView.shared.hideProgressView()
        }
    }
        
}
