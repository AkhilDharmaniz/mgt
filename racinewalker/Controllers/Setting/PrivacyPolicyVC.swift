//
//  PrivacyPolicyVC.swift
//  racinewalker
//
//  Created by Vivek Dharmani on 7/15/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit
import WebKit

class PrivacyPolicyVC: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = URL(string: "http://mgtquotes.com/privacy-policy.html") {
            let request = URLRequest(url: url)
            webView.load(request)
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backbuttonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
