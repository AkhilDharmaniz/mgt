//
//  ContactUsVC.swift
//  racinewalker
//
//  Created by Vivek Dharmani on 6/26/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit

class ContactUsVC: UIViewController {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    var message = String()
    @IBOutlet weak var contactUsTxtView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.getData()
        profileImage.contentMode = .scaleToFill
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = profileImage.frame.height/2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = profileImage.frame.height/2
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        if contactUsTxtView.text.isEmpty == true{
            ValidateData(strMessage: "Please enter something")
        }else{
            sendData()
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getData() {
        let id = UserDefaults.standard.value(forKey: "id") ?? ""
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            IJProgressView.shared.showProgressView()
            let signInUrl = Constant.shared.baseUrl + Constant.shared.userData
            print(signInUrl)
            let parms : [String:Any] = ["user_id":id]
            print(parms)
            AFWrapperClass.requestPOSTURL(signInUrl, params: parms, success: { (response) in
                IJProgressView.shared.hideProgressView()
                print(response)
                self.message = response["message"] as? String ?? ""
                if let status = response["status"] as? Int{
                    if status == 1{
                        print(response)
                        if let allData = response["user_details"] as? [String:Any]{
                            print(allData)
                            let firstName =  allData["first_name"] as? String ?? ""
                            let lastName =  allData["last_name"] as? String ?? ""
                            self.userName.text = "\(firstName) " + "\(lastName)"
                            let url = URL(string:allData["image"] as? String ?? "")
                            //                            UserDefaults.standard.set(url, forKey: "imgUrl")
                            if url != nil{
                                if let data = try? Data(contentsOf: url!)
                                {
                                    if let image: UIImage = (UIImage(data: data)){
                                        self.profileImage.image = image
                                        self.profileImage.contentMode = .scaleToFill
                                        IJProgressView.shared.hideProgressView()
                                    }
                                }
                            }
                            else{
                                self.profileImage.image = UIImage(named: "profileImage")
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constant.shared.appTitle, message: self.message, view: self)
                    }
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                print(error)
            }
            
        } else {
            print("Internet connection FAILED")
            alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
        }
    }
    
    func sendData() {
        let id = UserDefaults.standard.value(forKey: "id") ?? ""
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            IJProgressView.shared.showProgressView()
            let signInUrl = Constant.shared.baseUrl + Constant.shared.contactUs
            print(signInUrl)
            let parms : [String:Any] = ["user_id":id,"content":contactUsTxtView.text!]
            print(parms)
            AFWrapperClass.requestPOSTURL(signInUrl, params: parms, success: { (response) in
                IJProgressView.shared.hideProgressView()
                print(response)
                self.message = response["message"] as? String ?? ""
                if let status = response["status"] as? Int{
                    if status == 1{
                        print(response)
                        showAlertMessage(title: Constant.shared.appTitle, message: self.message, okButton: "Ok", controller: self) {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constant.shared.appTitle, message: self.message, view: self)
                    }
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                print(error)
            }
            
        } else {
            print("Internet connection FAILED")
            alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
        }
        
    }
}
