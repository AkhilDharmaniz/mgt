//
//  ResetPasswordVC.swift
//  racinewalker
//
//  Created by Vivek Dharmani on 6/25/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController {

    var password = String()
    var message = String()
    @IBOutlet weak var confirmPasswordTxtFld: UITextField!
    @IBOutlet weak var newPasswordTxtFld: UITextField!
    @IBOutlet weak var currentPasswordTxtFld: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func submitButtonAction(_ sender: Any) {
        
        
        if (currentPasswordTxtFld.text!.isEmpty){
            
            ValidateData(strMessage: "Please enter current password")
            
        }else if (password == currentPasswordTxtFld.text){
            
            ValidateData(strMessage: "Password matches")
            
        }else if (newPasswordTxtFld.text!.isEmpty){
            
            ValidateData(strMessage: "Please enter new password")
            
        }else if (newPasswordTxtFld!.text!.count) < 4 || (newPasswordTxtFld!.text!.count) > 15{
            
            ValidateData(strMessage: "Please enter minimum 4 digit password")
        }
        else if(confirmPasswordTxtFld.text!.isEmpty){
            
            ValidateData(strMessage: "Please enter confirm password")
            
        }
        else if newPasswordTxtFld.text != confirmPasswordTxtFld.text{
            
            ValidateData(strMessage: "Password mismatch")
            
        }else if currentPasswordTxtFld.text == confirmPasswordTxtFld.text{
            
            ValidateData(strMessage: "New & current password should be different")
            
        }else{
            self.changePassword()
        }
    }
    
    func changePassword()  {
        let id = UserDefaults.standard.value(forKey: "id") ?? ""
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            IJProgressView.shared.showProgressView()
            let signInUrl = Constant.shared.baseUrl + Constant.shared.resetPassword
            print(signInUrl)
            let parms : [String:Any] = [  "user_id":id,"current_password":currentPasswordTxtFld.text ?? "","new_password":newPasswordTxtFld.text ?? "","confirm_password":confirmPasswordTxtFld.text ?? ""]
            print(parms)
            AFWrapperClass.requestPOSTURL(signInUrl, params: parms, success: { (response) in
                IJProgressView.shared.hideProgressView()
                print(response)
                self.message = response["message"] as? String ?? ""
                if let status = response["status"] as? Int{
                    if status == 1{
                        print(response)
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constant.shared.appTitle, message: self.message, view: self)
                    }
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                print(error)
            }
            
        } else {
            print("Internet connection FAILED")
            alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
        }

    }
    
}

extension UIViewController {
    
    func ValidateData(strMessage: String)
    {
        let alert = UIAlertController(title: "mastergreatthoughts", message: strMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
