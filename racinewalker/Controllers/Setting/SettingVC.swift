//
//  SettingVC.swift
//  racinewalker
//
//  Created by Vivek Dharmani on 6/25/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit
import StoreKit

class SettingVC: UIViewController {
    
    @IBOutlet weak var subscriptionLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var settingButtonsTableView: UITableView!
    var message = String()
    var buttonsNames = ["Manage Subscription","Reset Password","About","Contact Us","Rate us","Instagram","Logout"]
    var iconImages = ["setting","unlock","info","call","star","insta","logout"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        profileImage.contentMode = .scaleToFill
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        self.getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = profileImage.frame.height/2
    }
    
    //   MARK:--> Button Actions
    
    
    @IBAction func gotoProfileVcButtonAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    
    func getData() {
        let id = UserDefaults.standard.value(forKey: "id") ?? ""
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            IJProgressView.shared.showProgressView()
            let signInUrl = Constant.shared.baseUrl + Constant.shared.userData
            print(signInUrl)
            let parms : [String:Any] = ["user_id":id]
            print(parms)
            AFWrapperClass.requestPOSTURL(signInUrl, params: parms, success: { (response) in
                IJProgressView.shared.hideProgressView()
                print(response)
                self.message = response["message"] as? String ?? ""
                if let status = response["status"] as? Int{
                    if status == 1{
                        print(response)
                        if let allData = response["user_details"] as? [String:Any]{
                            print(allData)
                            let firstName =  allData["first_name"] as? String ?? ""
                            let lastName =  allData["last_name"] as? String ?? ""
                            self.userNameLabel.text = "\(firstName) " + "\(lastName)"
                            
                            let plan = allData["purchasePlan"] as? String ?? ""
                            if plan == "monthly"{
                                self.subscriptionLabel.text = "Plus(\(allData["purchasePlan"] as? String ?? ""))"
                            }else if plan == "yearly"{
                              self.subscriptionLabel.text = "Gold(\(allData["purchasePlan"] as? String ?? ""))"
                            }else{
                              self.subscriptionLabel.text = "\(allData["purchasePlan"] as? String ?? "")"
                            }
                            let url = URL(string:allData["image"] as? String ?? "")
                            if url != nil{
                                if let data = try? Data(contentsOf: url!)
                                {
                                    if let image: UIImage = (UIImage(data: data)){
                                        self.profileImage.image = image
                                        self.profileImage.contentMode = .scaleToFill
                                        IJProgressView.shared.hideProgressView()
                                    }
                                }
                            }
                            else{
                                self.profileImage.image = UIImage(named: "profileImage")
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constant.shared.appTitle, message: self.message, view: self)
                    }
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                print(error)
            }
            
        } else {
            print("Internet connection FAILED")
            alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
        }
    }
    
    func logout() {
        let id = UserDefaults.standard.value(forKey: "id") ?? ""
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            IJProgressView.shared.showProgressView()
            let signInUrl = Constant.shared.baseUrl + Constant.shared.logout
            print(signInUrl)
            let parms : [String:Any] = ["user_id":id]
            print(parms)
            AFWrapperClass.requestPOSTURL(signInUrl, params: parms, success: { (response) in
                IJProgressView.shared.hideProgressView()
                print(response)
                self.message = response["message"] as? String ?? ""
                if let status = response["status"] as? Int{
                    if status == 1{
                        print(response)
                        UserDefaults.standard.removeObject(forKey: "subscribed")
                        if let _ = UserDefaults.standard.value(forKey: "pid"){
                            UserDefaults.standard.removeObject(forKey: "pid")
                        }
                        if let _ = UserDefaults.standard.value(forKey: "tid"){
                            UserDefaults.standard.removeObject(forKey: "tid")
                        }
                        if let _ = UserDefaults.standard.value(forKey: "noSubscriptionNeeded"){
                            UserDefaults.standard.removeObject(forKey: "noSubscriptionNeeded")
                        }
                        PurchaseHelper.shared.removePurchaseDefaults()
                        if let _ = UserDefaults.standard.value(forKey: "dateExpired"){
                            UserDefaults.standard.removeObject(forKey: "dateExpired")
                        }
                        if let _ = UserDefaults.standard.value(forKey: "freeVal"){
                            UserDefaults.standard.removeObject(forKey: "freeVal")
                        }
                        UserDefaults.standard.removeObject(forKey: "id")
                        let appDel = UIApplication.shared.delegate as! AppDelegate
                        appDel.Logout1()
                        
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constant.shared.appTitle, message: self.message, view: self)
                    }
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                print(error)
            }
            
        } else {
            print("Internet connection FAILED")
            alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
        }
    }
}

extension SettingVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return buttonsNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingButtonTableViewCell", for: indexPath) as? SettingButtonTableViewCell
        cell?.nameLabel.text = buttonsNames[indexPath.row]
        cell?.iconImages.image = UIImage(named: iconImages[indexPath.row])
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC
            vc?.fromSettings = true
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }else if indexPath.row == 1{
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ResetPasswordVC") as? ResetPasswordVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }else if indexPath.row == 2{
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AboutUSVC") as? AboutUSVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }else if indexPath.row == 3{
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ContactUsVC") as? ContactUsVC
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }else if indexPath.row == 4{
            rateApp(appId: "id1521350647")
//            if #available( iOS 10.3,*){
//                SKStoreReviewController.requestReview()
//            }else{
//                rateApp(appId: "id1521350647")
//            }
        }else if indexPath.row == 5{
           if let url = URL(string: "https://www.instagram.com/mastergreatthoughts/") {
               UIApplication.shared.open(url)
           }
        }else if indexPath.row == 6{
            let dialogMessage = UIAlertController(title: Constant.shared.appTitle, message: "Are you sure you want to Logout?", preferredStyle: .alert)
            
            // Create OK button with action handler
            let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                print("Ok button click...")
                self.logout()
                
            })
            
            // Create Cancel button with action handlder
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
                print("Cancel button click...")
            }
            
            //Add OK and Cancel button to dialog message
            dialogMessage.addAction(ok)
            dialogMessage.addAction(cancel)
            
            // Present dialog message to user
            self.present(dialogMessage, animated: true, completion: nil)
        }else{
            
        }
    }
    fileprivate func rateApp(appId: String) {
        openUrl("itms-apps://itunes.apple.com/app/" + appId)
    }
    fileprivate func openUrl(_ urlString:String) {
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}

class SettingButtonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImages: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
