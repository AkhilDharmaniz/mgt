//
//  SplashVC.swift
//  racinewalker
//
//  Created by IMac on 7/8/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit
import SwiftyStoreKit

class SplashVC: UIViewController {
var sharedSecret = "74d21d5506c94441910e758889d011d2"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if let pid = UserDefaults.standard.value(forKey: "pid") as? String{
            let tid = UserDefaults.standard.value(forKey: "subscribed") as? String
            if tid == "1"{
              //  verifyPurchase(product: pid)
            }
        }
    }
    func verifyPurchase(product: String){
           IJProgressView.shared.showProgressView()
           let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: sharedSecret)
           SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
               IJProgressView.shared.hideProgressView()
               switch result {
               case .success(let receipt):
                   let productId = product
                   let purchaseResult = SwiftyStoreKit.verifySubscription(
                       ofType: .autoRenewable,
                       productId: productId,
                       inReceipt: receipt)
                   switch purchaseResult {
                   case .purchased(let expiryDate, let items):
                       print("\(productId) is valid until \(expiryDate)\n\(items)\n")
                       
                       let timeStamp = (items[0].subscriptionExpirationDate)!.timeIntervalSince1970
                       print(timeStamp)
                       let fdate = Date(timeIntervalSince1970: timeStamp)
                       let formatter = DateFormatter()
                       formatter.dateFormat = "dd-MM-YYYY"
                       formatter.timeZone = .current
                       let expiryString = formatter.string(from: fdate)
                       print("Purchased Already will expire on \(expiryString)", items[0].transactionId)
                       let toDate = expiryDate
                       let fromDate = Calendar.current.date(byAdding: .month, value: -1, to: toDate)
                       //   let expiryString1 = formatter.string(from: fromDate!)
                       let todayDate = Date()
                       let formatter1 = DateFormatter()
                       formatter1.dateFormat = "dd-MM-YYYY"
                       formatter1.timeZone = .current
                       let expiryString1 = formatter1.string(from: todayDate)
                       PurchaseHelper.shared.setPurchaseDefaults(transactionDate: fromDate!, expiryDate: toDate, productId: productId)
//                       self.subscribe(productID: productId, productPlan: self.planString, timeStamp: expiryString1, transactionId: items[0].transactionId)
                       break
                   case .expired(let expiryDate, let items):
                       let formatter = DateFormatter()
                       formatter.dateFormat = "dd-MM-YYYY"
                       formatter.timeZone = .current
                       let expiryString = formatter.string(from: expiryDate)
                       print("\(productId) is expired since \(expiryString)\n\(items)\n")
                       let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC
                       self.navigationController?.pushViewController(vc!, animated: true)
                   // self.purchaseBtn.setTitle("Pay \(self.priceString)", for: .normal)
                   case .notPurchased:
                       print("The user has never purchased \(productId)")
                   }
               case .error(let error):
                print(error.localizedDescription)
                //   alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
               }
           }
       }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if let userId = UserDefaults.standard.value(forKey: "id") as? String{
            if userId != ""{
                let url = Constant.shared.baseUrl + Constant.shared.CheckSubscriptionPlan
                let uid = UserDefaults.standard.value(forKey: "id") as! String
                let params = ["user_id":uid,"purchasePlan":"","expiredatetime":""] as [String : Any]
                print(params)
             //   IJProgressView.shared.showProgressView()
                AFWrapperClass.requestPOSTURL(url, params: params, success: { (dict) in
                    print("subscribed response is", dict)
                //    IJProgressView.shared.hideProgressView()
                    if let status = dict["status"] as? Int{
                        if status == 1{
                            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBarVC") as? TabBarVC
                            self.navigationController?.pushViewController(vc!, animated: false)
                        }else{
                           let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SubscriptionVC") as? SubscriptionVC
                            self.navigationController?.pushViewController(vc!, animated: false)
                        }
                    }
                }) { (error) in
                    print(error.localizedDescription)
                }
            }
        }else{
         let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignInVC") as? SignInVC
         self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
}
