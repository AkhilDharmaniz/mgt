//
//  HomeVC.swift
//  racinewalker
//
//  Created by Vivek Dharmani on 6/25/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    
    var quotesArray = [String]()
    var message = String()
    var quotesDataArray = [QuotesData]()
    var pageNo = 1
    var arrCount = 0
    @IBOutlet weak var quotesTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        quotesArray = ["a","b","c","d","e","f","g","h"]
        quotesTableView.estimatedRowHeight = 44.0
        quotesTableView.rowHeight = UITableView.automaticDimension
        // Do any additional setup after loading the view.
       
           
    }
    override func viewDidAppear(_ animated: Bool) {
        self.quotesDataArray.removeAll()
        self.pageNo = 1
        getData()

    }
    func getData()  {
        let id = UserDefaults.standard.value(forKey: "id") ?? ""
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            IJProgressView.shared.showProgressView()
            let signInUrl = Constant.shared.baseUrl + Constant.shared.quotesData
            print(signInUrl)
            let parms : [String:Any] = ["user_id":id,"pageno": pageNo, "per_page" : "15"]
            print(parms)
            AFWrapperClass.requestPOSTURL(signInUrl, params: parms, success: { (response) in
                IJProgressView.shared.hideProgressView()
                print(response)
                self.message = response["message"] as? String ?? ""
              //  self.quotesDataArray.removeAll()
                if let status = response["status"] as? Int{
                    if status == 1{
                        print(response)
                        let allData = response["Quotes_detail"] as? [AnyObject]
                        if let dataDict = allData as? [[String:Any]]{
                            print(dataDict)
                            self.arrCount = dataDict.count
                            for i in 0..<dataDict.count{
                                self.quotesDataArray.append(QuotesData(authorName: dataDict[i]["author"] as? String ?? "", quote: dataDict[i]["content"] as? String ?? "", bg_image: dataDict[i]["bg_image"] as? String ?? "", color: dataDict[i]["color"] as? String ?? "", image: dataDict[i]["image"] as? String ?? ""))
                            }
                            print(self.quotesDataArray)
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constant.shared.appTitle, message: self.message, view: self)
                    }
                    self.quotesTableView.reloadData()
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                print(error)
            }
            
        } else {
            print("Internet connection FAILED")
            alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
        }

    }
    
}
extension HomeVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quotesDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuotesTableViewCell", for: indexPath) as? QuotesTableViewCell
        cell?.dataView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cell?.quotesLabel.text = quotesDataArray[indexPath.row].quote
        cell?.authorName.text = quotesDataArray[indexPath.row].authorName
        cell?.quotesLabel.textColor = UIColor(hexaRGB: quotesDataArray[indexPath.row].color)
//        let indexPath1 = IndexPath(row: indexPath.row, section: 0)
      
        cell?.bgImg.sd_setImage(with: URL(string: quotesDataArray[indexPath.row].image), placeholderImage: UIImage(named: ""))
        DispatchQueue.main.async {
            let frame = tableView.rectForRow(at: indexPath)
            cell?.imgHeight.constant = frame.size.height
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == quotesDataArray.count-1 {
            if arrCount == 15 {
                pageNo = pageNo + 1
                self.getData()
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "QuoteVC") as! QuoteVC
        vc.quoteImgStr = quotesDataArray[indexPath.row].bg_image
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
class QuotesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var quotesLabel: UILabel!
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var bgImg: UIImageView!
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
struct QuotesData {
    var authorName : String
    var quote : String
    var bg_image : String
    var color : String
    var image : String
    
    init(authorName : String, quote : String, bg_image : String, color : String, image : String) {
        self.authorName = authorName
        self.quote = quote
        self.bg_image = bg_image
        self.color = color
        self.image = image
    }
}
