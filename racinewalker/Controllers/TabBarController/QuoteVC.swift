//
//  QuoteVC.swift
//  racinewalker
//
//  Created by IMac on 8/25/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit
import SDWebImage

class QuoteVC: UIViewController {

    @IBOutlet weak var quoteImg: UIImageView!
    var quoteImgStr = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        let userImage = quoteImgStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
    self.quoteImg.sd_setImage(with: URL(string: userImage), placeholderImage: UIImage(named: ""))
        
    }
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func downloadBtnAction(_ sender: UIButton) {
        guard let image = quoteImg.image else { return }
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    @IBAction func shareBtnAction(_ sender: UIButton) {
      //  let shareText = "MGT:\n\nPlease check your bin barcode now you can take print this QR Code\n\n \(qrCodeImagesArray[indexPath.row])"
        
        IJProgressView.shared.showProgressView()
        let userImage = quoteImgStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        if let url = URL(string: userImage),
            let data = try? Data(contentsOf: url),
            let image = UIImage(data: data) {
            IJProgressView.shared.hideProgressView()
            let vc = UIActivityViewController(activityItems: [image], applicationActivities: [])
            present(vc, animated: true)
        }
    }
}
