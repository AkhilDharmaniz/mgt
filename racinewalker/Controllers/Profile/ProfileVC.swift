//
//  ProfileVC.swift
//  racinewalker
//
//  Created by Vivek Dharmani on 6/25/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    var message = String()
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var passwordTxtFld: UITextField!
    @IBOutlet weak var genderTxtFld: UITextField!
    @IBOutlet weak var emailTxtFld: UITextField!
    @IBOutlet weak var usernameTxtFld: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTxtFld.isUserInteractionEnabled = false
        emailTxtFld.isUserInteractionEnabled = false
        passwordTxtFld.isUserInteractionEnabled = false
        genderTxtFld.isUserInteractionEnabled = false


        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
          getData()
          profileImage.contentMode = .scaleToFill
          profileImage.layer.masksToBounds = true
          profileImage.layer.cornerRadius = profileImage.frame.height/2
      }
      
      override func viewWillAppear(_ animated: Bool) {
          profileImage.layer.masksToBounds = true
          profileImage.layer.cornerRadius = profileImage.frame.height/2
      }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editButtonAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func getData() {
        let id = UserDefaults.standard.value(forKey: "id") ?? ""
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            IJProgressView.shared.showProgressView()
            let signInUrl = Constant.shared.baseUrl + Constant.shared.userData
            print(signInUrl)
            let parms : [String:Any] = ["user_id":id]
            print(parms)
            AFWrapperClass.requestPOSTURL(signInUrl, params: parms, success: { (response) in
                IJProgressView.shared.hideProgressView()
                print(response)
                self.message = response["message"] as? String ?? ""
                if let status = response["status"] as? Int{
                    if status == 1{
                        print(response)
                        if let allData = response["user_details"] as? [String:Any]{
                            print(allData)
                            let firstName =  allData["first_name"] as? String ?? ""
                            let lastName =  allData["last_name"] as? String ?? ""
                            let userName = firstName + lastName
                            self.usernameTxtFld.text = userName
                            self.emailTxtFld.text = allData["email"] as? String ?? ""
                            self.genderTxtFld.text = allData["gender"] as? String ?? ""
                            self.passwordTxtFld.text = "******"
                            let url = URL(string:allData["image"] as? String ?? "")
//                            UserDefaults.standard.set(url, forKey: "imgUrl")
                            if url != nil{
                                if let data = try? Data(contentsOf: url!)
                                {
                                    if let image: UIImage = (UIImage(data: data)){
                                        self.profileImage.image = image
                                        self.profileImage.contentMode = .scaleToFill
                                        IJProgressView.shared.hideProgressView()
                                    }
                                }
                            }
                            else{
                                self.profileImage.image = UIImage(named: "profileImage")
                            }                            
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constant.shared.appTitle, message: self.message, view: self)
                    }
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                print(error)
            }
            
        } else {
            print("Internet connection FAILED")
            alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
        }
    }
}
