//
//  EditProfileVC.swift
//  racinewalker
//
//  Created by Vivek Dharmani on 6/25/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource {
    
    var message = String()
    var imagePicker = UIImagePickerController()
    var base64String = String()
    var userName = String()
    var firstName = String()
    var lastName = String()
    var selectedValue = String()
    var pickerDataArray = [String]()
    var genderPickerView = UIPickerView()
    var password = String()
    
    @IBOutlet weak var passworddTxtFld: UITextField!
    @IBOutlet weak var genderTxtFld: UITextField!
    @IBOutlet weak var emailTxtFld: UITextField!
    @IBOutlet weak var usernameTxtFld: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerDataArray = ["Male","Female"]
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        getData()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        
        profileImage.contentMode = .scaleToFill
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = profileImage.frame.height/2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = profileImage.frame.height/2
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        if (usernameTxtFld.text?.isEmpty)!{
            ValidateData(strMessage: " Please enter username")
        }
        else if (emailTxtFld.text?.isEmpty)!{
            ValidateData(strMessage: " Please enter email")
        }
        else if isValidEmail(testStr: (emailTxtFld.text)!) == false{
            ValidateData(strMessage: "Enter valid email")
        }
        else if (genderTxtFld.text?.isEmpty)!{
            ValidateData(strMessage: " Please enter gender")
        }else{
            updateUserData()
        }

    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeProfileImage(_ sender: Any) {
        self.showActionSheet()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        genderTxtFld.inputView = genderPickerView
    }
    
    func getData() {
        let id = UserDefaults.standard.value(forKey: "id") ?? ""
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            IJProgressView.shared.showProgressView()
            let signInUrl = Constant.shared.baseUrl + Constant.shared.userData
            print(signInUrl)
            let parms : [String:Any] = ["user_id":id]
            print(parms)
            AFWrapperClass.requestPOSTURL(signInUrl, params: parms, success: { (response) in
                IJProgressView.shared.hideProgressView()
                print(response)
                self.message = response["message"] as? String ?? ""
                if let status = response["status"] as? Int{
                    if status == 1{
                        print(response)
                        if let allData = response["user_details"] as? [String:Any]{
                            print(allData)
                            self.firstName =  allData["first_name"] as? String ?? ""
                            self.lastName =  allData["last_name"] as? String ?? ""
                            self.userName = self.firstName + self.lastName
                            self.usernameTxtFld.text = self.userName
                            self.emailTxtFld.text = allData["email"] as? String ?? ""
                            self.genderTxtFld.text = allData["gender"] as? String ?? ""
                            self.password = allData["password"] as? String ?? ""
                            let url = URL(string:allData["image"] as? String ?? "")
                            if url != nil{
                                if let data = try? Data(contentsOf: url!)
                                {
                                    if let image: UIImage = (UIImage(data: data)){
                                        self.profileImage.image = image
                                        self.profileImage.contentMode = .scaleToFill
                                        IJProgressView.shared.hideProgressView()
                                    }
                                }
                            }
                            else{
                                self.profileImage.image = UIImage(named: "profileImage")
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constant.shared.appTitle, message: self.message, view: self)
                    }
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                print(error)
            }
            
        } else {
            print("Internet connection FAILED")
            alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
        }
    }
    

    
    func updateUserData() {
        let id = UserDefaults.standard.value(forKey: "id") ?? ""
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            IJProgressView.shared.showProgressView()
            let signInUrl = Constant.shared.baseUrl + Constant.shared.addUserDetails
            print(signInUrl)
            let result = "\(usernameTxtFld.text ?? "")"
            let userName = result.split{ !$0.isLetter }
            print(userName)
            let parms : [String:Any]
            if userName.count == 1 {
                 parms  = [ "first_name":userName[0],
                            "email":emailTxtFld.text!,
                            "gender":genderTxtFld.text!,
                            "password":self.password,
                            "image":base64String,
                            "user_id":id]
            }
            else{
                
                parms = [   "first_name":userName[0],
                            "last_name":userName[1],
                            "email":emailTxtFld.text!,
                            "gender":genderTxtFld.text!,
                            "password":self.password,
                            "image":base64String,
                            "user_id":id]
            }
            print(parms)
            AFWrapperClass.requestPOSTURL(signInUrl, params: parms, success: { (response) in
                IJProgressView.shared.hideProgressView()
                print(response)
                self.message = response["message"] as? String ?? ""
                if let status = response["status"] as? Int{
                    if status == 1{
                        print(response)
                        self.popViewControllerss(popViews: 2)
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constant.shared.appTitle, message: self.message, view: self)
                    }
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constant.shared.appTitle, message: error.localizedDescription, view: self)
                print(error)
            }
            
        } else {
            print("Internet connection FAILED")
            alert(Constant.shared.appTitle, message: "Check internet connection", view: self)
        }
    }
    
    
    func popViewControllerss(popViews: Int, animated: Bool = true) {
          if self.navigationController!.viewControllers.count > popViews
          {
              let vc = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - popViews - 1]
               self.navigationController?.popToViewController(vc, animated: animated)
          }
      }
    
    func showActionSheet(){
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: NSLocalizedString("Upload Image", comment: ""), message: nil, preferredStyle: .actionSheet)
        actionSheetController.view.tintColor = UIColor.black
        let cancelActionButton: UIAlertAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: NSLocalizedString("Take Photo", comment: ""), style: .default)
        { action -> Void in
            self.camera()
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: NSLocalizedString("Choose From Gallery", comment: ""), style: .default)
        { action -> Void in
            self.gallery()
        }
        actionSheetController.addAction(deleteActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    func camera()
    {
        let myPickerControllerCamera = UIImagePickerController()
        myPickerControllerCamera.delegate = self
        myPickerControllerCamera.sourceType = UIImagePickerController.SourceType.camera
        myPickerControllerCamera.allowsEditing = true
        self.present(myPickerControllerCamera, animated: true, completion: nil)
        
    }
    
    func gallery()
    {
        
        let myPickerControllerGallery = UIImagePickerController()
        myPickerControllerGallery.delegate = self
        myPickerControllerGallery.sourceType = UIImagePickerController.SourceType.photoLibrary
        myPickerControllerGallery.allowsEditing = true
        self.present(myPickerControllerGallery, animated: true, completion: nil)
        
    }
    
    
    //MARK:- ***************  UIImagePickerController delegate Methods ****************
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //        guard let image = info[UIImagePickerController.InfoKey.originalImage]
        guard let image = info[UIImagePickerController.InfoKey.editedImage]
            as? UIImage else {
                return
        }
        //        let imgData3 = image.jpegData(compressionQuality: 0.4)
        self.profileImage.contentMode = .scaleToFill
        self.profileImage.image = image
        
        guard let imgData3 = image.jpegData(compressionQuality: 0.2) else {return}
        base64String = imgData3.base64EncodedString(options: .lineLength64Characters)
        UserDefaults.standard.set(base64String, forKey: "image")
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.imagePicker = UIImagePickerController()
        dismiss(animated: true, completion: nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataArray.count
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderTxtFld.text = pickerDataArray[row]
        
    }
    func pickerView(_ pickerView: UIPickerView, diddeSelect row: Int, inComponent component: Int) {
        genderPickerView.isHidden = true
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = (view as? UILabel) ?? UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont(name: "Lato-Bold", size: 17)
        label.text = pickerDataArray[row]
        return label
    }
}

