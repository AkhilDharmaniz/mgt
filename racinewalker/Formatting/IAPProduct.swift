//
//  IAPProduct.swift
//  Arox
//
//  Created by apple on 8/22/19.
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
enum IAPProduct: String{
    case goldPlan = "com.mastergreatthoughts.app.goldPlan"
    case plusPlan = "com.mastergreatthoughts.app.plusPlan"
}
